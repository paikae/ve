var tipType = "tipType";
var codesKey = "codes" ;
var copyKey = "copyCodes" ;
var curType ;
fillPage();

function fillPage() {
	curType = localStorage.getItem(tipType) ;
	if(!curType) {
		curType = "10";
	}
	codesKey = "codes" + curType ;
	copyKey = "copyCodes" + curType ;

	var showLCode = showLocalData(codesKey);
	var showCCode = showLocalData(copyKey);
	document.getElementById("codes").innerHTML = showLCode ;
	document.getElementById("copyCodes").innerHTML = showCCode ;
	
	document.getElementById("tipType").textContent = '当前是'+curType+'0礼包' ;
}

function showLocalData(key) {
	var localCodes = localStorage.getItem(key) ;
	var showData = '' ;
	if(localCodes) {
		var localLine = localCodes.match(/[^\r\n]+/g);
		for(var i = 0; i < localLine.length ; i++) {
			var val = localLine[i];
			showData += (i+1) + "." + val + '<br/>';
		}
	}
	return showData;
}
function addCodes() {
	var codeText = document.getElementById("codeText").value ;
	if(codeText.length <= 0) {
		alert("请粘贴要添加的内容");
		return false ;		
	}
	codeText = codeText.trim();
	var moneyNum = getCodePrefix(codeText) ;
	if(curType != moneyNum) {
		alert('添加错误,当前是'+curType+'0礼包');
		return false;
	}
	addOriginal(codeText, 0) ;
	
	document.getElementById("codeText").value = '';
	alert("添加成功");
}

function addOriginal(codeText, typeKey) {
	var cke = new Set();
	
	if(typeKey == 0) {
		setLocaCode(cke);
	}
	
	var arrCodes = codeText.match(/[^\r\n]+/g);
	for(var i = 0; i < arrCodes.length ; i++) {
		var val = arrCodes[i].trim();
		if(val == '') continue;
		
		var moneyNum = getCodePrefix(val) ;
		if(curType != moneyNum) {
			alert('添加错误,当前是'+curType+'0礼包');
			return false;
		}
		cke.add(val) ;
	}
	
	if(typeKey != 0) {
		setLocaCode(cke);
	}
	
	var lineCodes = '' ;
	var showCodes = '' ;
	
	var index = 1;
	for (let val of cke) {
		lineCodes += val + '\r\n';
		showCodes += index + "." + val + '<br/>';
		index++;
	}
	document.getElementById("codes").innerHTML = showCodes ;
	localStorage.setItem(codesKey, lineCodes) ;
}

function setLocaCode(cke) {
	var localCodes = localStorage.getItem(codesKey) ;
	if(localCodes) {
		var localLine = localCodes.match(/[^\r\n]+/g);
		for(var i = 0; i < localLine.length ; i++) {
			var val = localLine[i];
			cke.add(val) ;
		}
	}
}

function clearCodes() {
	var isDel = confirm("你确定要清空数据吗?");
    if(isDel) {
    	localStorage.removeItem(codesKey);
		document.getElementById("codes").innerHTML = '' ;
		
		localStorage.removeItem(copyKey);
		document.getElementById("copyCodes").innerHTML = '' ;
    }
}

function getCodePrefix(code) {
	var partCode = code.substring(0, 6);
	const regex = /\d+/g;
	return partCode.match(regex).map(Number);
}

function resetCodes() {
	var copyCodes = localStorage.getItem(copyKey) ;
	if(!copyCodes) {
		alert("没有撤消的数据");
		return false;
	}
	var isDel = confirm("你确定要撤消数据吗?");
    if(isDel) {
		addOriginal(copyCodes, 1) ;
		
		localStorage.removeItem(copyKey);
		document.getElementById("copyCodes").innerHTML = '' ;
    }
}

function cutCodes() {
	var domNum = document.getElementById("num") ;
	var num = domNum.value;
	if(!num || num <= 0) {
		alert("请输入条数");
		return false; 
	}
	var localCodes = localStorage.getItem(codesKey) ;
	if(!localCodes) {
		alert("已经没有数据了,请添加数据");
		return false; 
	}
	var localLine = localCodes.match(/[^\r\n]+/g);
	if(localLine.length < num) {
		alert("数据不足,请添加数据");
		return false; 
	}
	domNum.value = '' ;
	
	var cutData = '';
	var showCurData = '' ;
	var leaveData = '';
	var showLeaveData = '' ;
	
	var index = 1;
	for(var i = 0; i < localLine.length ; i++) {
		var val = localLine[i];
		if(i < num) {
			cutData += val ;
			if(i != (num-1)) {
				cutData += '\r\n' ;
			} 
			showCurData += (i+1) + "." + val + '<br/>';
		} else {
			leaveData += val + '\r\n';
			showLeaveData += index + "." + val + '<br/>';
			index++;
		}
	}
	localStorage.setItem(codesKey, leaveData) ;
	localStorage.setItem(copyKey, cutData) ;
	
	document.getElementById("codes").innerHTML = showLeaveData ;
	document.getElementById("copyCodes").innerHTML = showCurData ;
	
	var hideCodes = document.getElementById('hideCodes') ;
	hideCodes.textContent = cutData ;
	hideCodes.select();
    document.execCommand('copy');
    hideCodes.textContent = '' ;
    hideCodes.blur();
	alert("剪切成功");
}

function setType(val) {
	localStorage.setItem(tipType, val) ;
	fillPage();
}
